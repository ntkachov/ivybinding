package com.thedailynerd.ivybinding.builder;

import android.view.View;

/**
 * Created by Nick on 7/4/2016.
 */
public interface OnBindListener {
    void onBind(View inflatedView, Object dataItem);
}
