package com.thedailynerd.ivybinding.builder

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.thedailynerd.ivybinding.layout.IvyAutoLayout
import com.thedailynerd.ivybinding.IvyBinder
import com.thedailynerd.ivybinding.builder.OnBindListener
import com.thedailynerd.ivybinding.builder.RenderTarget

import java.util.LinkedList

/**
 * Ideal:
 * IvyBinder.create().with(R.id.repo).~~render(Repo.class)~~.bindTo(BR.repo)
 */
class IvyBuilder(private val result: IvyBinder) {

    val renderTargets = LinkedList<RenderTarget>()
    lateinit var rootLayout: ViewGroup
    private var  impliedContext: Context? = null

    fun render(targetClass: Class<*>): IvyBuilderItemBuilder {
        return IvyBuilderItemBuilder(targetClass, this)
    }

    fun inside(targetClass: Class<*>) : IvyBuilder {
        return IvyBuilder(result)
    }

    fun generateBindings(layout: ViewGroup) : IvyBinder {
        rootLayout = layout
        result.setRootLayout(rootLayout)
        populateBindings()
        layout.post { //We create views here. Post this to the main thread
            result.onReadyToConsume()
        }
        return result
    }

    fun generateBindings(activity: Activity){
        val activityRoot = activity.findViewById(android.R.id.content)
        generate(activityRoot)
    }

    fun generateBindings(fragment: Fragment){
        val fragmentRoot = fragment.view
        generate(fragmentRoot)
    }

    private fun generate(view: View){
        if(view is ViewGroup) {
            generateBindings(view)
        } else {
            throw IllegalStateException("Provided view was not a ViewGroup, your Fragment or activity" +
                    " root must be a ViewGroup. Please explicitly specify which ViewGroup you would" +
                    "like to use as the layout hierarchy root")
        }
    }

    private fun populateBindings(){
        renderTargets.forEach { render ->
            result.bindingMap.put(render.targetClass, render)
        }
    }

    internal fun addRenderTarget(renderTarget: RenderTarget){
        renderTargets.add(renderTarget)
    }


}
