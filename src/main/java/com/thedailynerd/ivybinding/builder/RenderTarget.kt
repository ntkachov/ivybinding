package com.thedailynerd.ivybinding.builder

import android.view.View
import android.view.ViewGroup
import java.util.*

/**
 * Created by Nick on 7/2/16.
 */
class RenderTarget(val targetClass: Class<*>) {

    var targetBindingVar: Int? = null
    var targetBindingLayout: Int? = null
    var targetBindingHandler : LinkedList<OnBindListener> = LinkedList() //TODO: Would by Lazy be better here?
    var targetBindConditional: ((Any?) -> Boolean)? = null
    val targetClickListener: LinkedList<View.OnClickListener> = LinkedList()
    var parentViewGroup: ViewGroup? = null
    var parentViewId : Int? = null
}