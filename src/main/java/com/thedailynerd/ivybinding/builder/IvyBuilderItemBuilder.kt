package com.thedailynerd.ivybinding.builder

import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.view.View
import android.view.ViewGroup

/**
 * RenderBuilder helps guide the developer to assign the correct class to the binding variable.
 * RenderBuilder creates a RenderTarget that will assign a BindingTarget to the Ivy for inflation
 */
class IvyBuilderItemBuilder internal constructor(private val buildTargetClass: Class<*>, private val ivyBuilder: IvyBuilder) {

    private val renderTarget : RenderTarget = RenderTarget(buildTargetClass)

    /**
     * Runs whenever the class is bound to the object. OnBindListener is guaranteed to be called right after the
     * object is bound with the view.
     */
    fun onBind(onBindListener: OnBindListener): IvyBuilderItemBuilder {
        renderTarget.targetBindingHandler.add(onBindListener)
        return this
    }

    fun onBind(bindListener: (View, Any?) -> Unit) : IvyBuilderItemBuilder {
        //OnBindListener(bindListener) basically creates a OnBindListener and delegates it to the lambda
        renderTarget.targetBindingHandler.add(OnBindListener(bindListener))
        return this
    }

    /**
     * Creates a conditional that will evaluate if the view should be bound at all. If this returns false,
     * the class will skip binding entirely
     */
    fun bindOnlyIf(bindConditional : (Any?) -> Boolean): IvyBuilderItemBuilder {
        renderTarget.targetBindConditional = bindConditional
        return this
    }

    /**
     * If the target class extends OnBindListener this will tell it to run that listener when
     * the class is bound to its target. This is NOT the default behavior because we want this
     * behavior to be called out explicitly when it is used.
     */
    fun automaticallyCallBinderFor(targetClass : Class<*>): IvyBuilderItemBuilder {
        //TODO: Figure out how to test targetClass implements OnBindListener outside of the bindingHandler
        //    Kotlin doesn't like OnBindListener.class.isAssignableFrom(targetClass)
        renderTarget.targetBindingHandler.add(OnBindListener { view, any ->
            if (any.javaClass.isInstance(targetClass)) {
                if (any is OnBindListener) {
                    any.onBind(view, any)
                } else {
                    throw IllegalArgumentException("automaticallyCallBinderFor requires a class that extends OnBindListener")
                }
            }
        })
        return this
    }

    fun onClick(clickListener: View.OnClickListener): IvyBuilderItemBuilder {
        renderTarget.targetClickListener.add(clickListener)
        return this
    }

    fun onClick(clickListener: (View) -> Unit) : IvyBuilderItemBuilder {
        renderTarget.targetClickListener.add(View.OnClickListener(clickListener))
        return this
    }

    fun bindTo(@LayoutRes layout : Int, brBindingVariable : Int): IvyBuilderItemBuilder {
        if(brBindingVariable == 0){
            throw IllegalArgumentException("brBindingVariable must be initialized and must not be 0")
        }
        renderTarget.targetBindingLayout = layout
        renderTarget.targetBindingVar = brBindingVariable
        return this
    }

    fun inflateInto(viewGroup : ViewGroup) : IvyBuilder {
        renderTarget.parentViewGroup = viewGroup
        ivyBuilder.addRenderTarget(renderTarget)
        return ivyBuilder
    }

    fun inflateInto(@IdRes viewTarget : Int) : IvyBuilder {
        renderTarget.parentViewId = viewTarget
        ivyBuilder.addRenderTarget(renderTarget)
        return ivyBuilder
    }


}