package com.thedailynerd.ivybinding.binderimpls

import com.google.android.agera.Repository
import com.google.android.agera.Updatable
import com.thedailynerd.ivybinding.IvyBinder

/**
 * Created by Nick on 8/13/2016.
 */
internal class ArrayIvyBinder(internal val dataArray : Array<*>) : IvyBinder() {

    override fun onReadyToConsume() {
        onDataChanged(dataArray)
    }

}
