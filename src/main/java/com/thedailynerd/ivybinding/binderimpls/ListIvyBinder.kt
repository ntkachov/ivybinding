package com.thedailynerd.ivybinding.binderimpls

import com.thedailynerd.ivybinding.IvyBinder

/**
 * Created by Nick on 8/13/2016.
 */
internal class ListIvyBinder(internal val dataList : List<*>) : IvyBinder() {

    override fun onReadyToConsume() {
        onDataChanged(dataList)
    }

}