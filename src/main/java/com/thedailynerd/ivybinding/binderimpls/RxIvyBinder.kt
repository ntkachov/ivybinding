package com.thedailynerd.ivybinding.binderimpls

import com.thedailynerd.ivybinding.IvyBinder
import rx.Observable

/**
 * Created by Nick on 7/25/2016.
 */
class RxIvyBinder(val observable: Observable<*>) : IvyBinder() {

    override fun onReadyToConsume() {
        observable.subscribe { anyNextItem ->
            onDataChanged(anyNextItem)
        }
    }


}
