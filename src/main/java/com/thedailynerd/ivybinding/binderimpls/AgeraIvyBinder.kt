package com.thedailynerd.ivybinding.binderimpls

import android.util.Log
import com.google.android.agera.Observable
import com.google.android.agera.Repository
import com.google.android.agera.Supplier
import com.google.android.agera.Updatable
import com.thedailynerd.ivybinding.IvyBinder

/**
 * Created by Nick on 7/4/2016.
 */
internal class AgeraIvyBinder(internal val dataProvider: Repository<*>) : IvyBinder(), Updatable {

    private val TAG = "AgeraIvyBinder"

    override fun onReadyToConsume() {
        dataProvider.addUpdatable(this)
    }

    override fun update() {
        Log.d(TAG, "Recieved Update from dataprovider")
        onDataChanged(dataProvider.get())
    }
}