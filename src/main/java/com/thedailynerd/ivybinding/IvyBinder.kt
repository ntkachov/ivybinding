package com.thedailynerd.ivybinding

import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.google.android.agera.Repository
import com.thedailynerd.ivybinding.binderimpls.AgeraIvyBinder
import com.thedailynerd.ivybinding.binderimpls.ArrayIvyBinder
import com.thedailynerd.ivybinding.binderimpls.ListIvyBinder
import com.thedailynerd.ivybinding.binderimpls.RxIvyBinder
import com.thedailynerd.ivybinding.builder.IvyBuilder
import com.thedailynerd.ivybinding.builder.RenderTarget
import com.thedailynerd.ivybinding.layout.BindingPair
import com.thedailynerd.ivybinding.layout.IvyBindingViewGenerator
import rx.Observable
import java.util.HashMap

/**
 * Main Ivy class. This class contains all of the calls necessary to start Ivy
 */
abstract class IvyBinder {

    abstract fun onReadyToConsume()

    val bindingMap = HashMap<Class<*>, RenderTarget>()
    private var mRenderConverter: IvyBindingViewGenerator? = null

    fun setRootLayout(rootLayout: ViewGroup) {
        mRenderConverter = IvyBindingViewGenerator(rootLayout)
    }

    protected fun onDataChanged(data: Any?) {
        if (data is Iterable<Any?>) {
           convertDataCollectionToViews(data)
        } else if(data is Array<*>){
            convertDataCollectionToViews(data.asIterable())
        } else {
            convertDataItemtoView(data)
        }
    }

    private fun convertDataCollectionToViews(data : Iterable<Any?>){
        //convert all of the render targets to bindings and filter out any that ended up null.
        val bindingsList : List<BindingPair> = data
                .map {
                    convertDataItemToRenderTarget(it)
                }.filter{
                    !it.isNull()
                }
        bindingsList.forEach { bindingPair ->
            createViewForBindingPair(bindingPair)
        }
    }

    private fun convertDataItemtoView(data : Any?){
        val dataItem = convertDataItemToRenderTarget(data)
        dataItem.isNotNull {
            createViewForBindingPair(it)
        }
    }

    fun convertDataItemToRenderTarget(item :Any?): BindingPair {
        if(item == null){
            return BindingPair.NULL
        }

        val modelClass = item.javaClass
        val renderTarget = bindingMap[modelClass]
        checkRenderTargetNotNull(renderTarget, modelClass)
        return BindingPair(renderTarget, item)
    }

    fun createViewForBindingPair(renderTarget: BindingPair){
        val renderConverterInstance = mRenderConverter
        if(renderConverterInstance != null){
             renderTarget.validate()
             renderConverterInstance.renderAndAttachRenderTargetToParent(renderTarget)
        } else {
            //TODO: Add message for how to fix this issue
            Log.e(TAG, "IvyBinder was never provided with a ViewGroup")
        }
    }

    private fun checkRenderTargetNotNull(renderTarget : RenderTarget?, modelClass : Class<*>){
        if (renderTarget == null) {
            //TODO: Add steps on how to fix this issue.
            Log.e(TAG, "Could not bind " + modelClass.canonicalName + ". No layout for this class was found")
        }
    }

    companion object {

        internal val TAG = "IvyBinder"

        /**
         * Create a new IvyBinding Builder given a repository
         */
        @JvmStatic
        fun initializeWith(repository: Repository<*>): IvyBuilder {
            return IvyBuilder(AgeraIvyBinder(repository))
        }

        /**
         * Creates a new IvyBinding Builder given an Observable.
         */
        @JvmStatic
        fun initializeWith(observable: Observable<*>) : IvyBuilder{
            return IvyBuilder(RxIvyBinder(observable))
        }

        /**
         * Creates a new IvyBinding Builder given an Array.
         */
        @JvmStatic
        fun initializeWith(array: Array<*>) : IvyBuilder{
            return IvyBuilder(ArrayIvyBinder(array))
        }

        /**
         * Creates a new IvyBinding Builder given a List.
         */
        @JvmStatic
        fun initializeWith(list: List<*>) : IvyBuilder{
            return IvyBuilder(ListIvyBinder(list))
        }
    }



}


