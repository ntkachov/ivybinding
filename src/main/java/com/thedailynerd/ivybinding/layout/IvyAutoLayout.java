package com.thedailynerd.ivybinding.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.thedailynerd.ivybinding.R;
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewGroupViewInflationAdapter;
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewInflationAdapter;
import com.thedailynerd.ivybinding.layout.recyclerview.IvyRecyclerViewAdapter;

/**
 * Created by Nick on 7/2/16.
 */
public class IvyAutoLayout extends FrameLayout{

    private static final int DEFAULT_STYLE__ATTR = 0, DEFAULT_STYLE_RES = 0;
    private static final int INVALID_VIEW_RES = 0;
    private int initialViewRes, emptyViewRes, groupViewRes;
    private ViewInflationAdapter layoutStratagy;
    private IvyBindingViewGenerator renderConverter;

    public IvyAutoLayout(Context context) {
        super(context);
    }

    public IvyAutoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initWithAttributes(attrs, DEFAULT_STYLE__ATTR, DEFAULT_STYLE_RES);
    }

    public IvyAutoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initWithAttributes(attrs, defStyleAttr, DEFAULT_STYLE_RES);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public IvyAutoLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initWithAttributes(attrs, defStyleAttr, defStyleRes);
    }

    private void initWithAttributes(AttributeSet attrs, int defaultStyleAttribute, int defaultStyleRes){
        TypedArray typedArray = getContext().getTheme()
                .obtainStyledAttributes(attrs, R.styleable.IvyAutoLayout, defaultStyleAttribute, defaultStyleRes);
        setInitialView(typedArray);
        setEmptyView(typedArray);
        typedArray.recycle();
    }

    private void setInitialView(TypedArray typedArray){
        initialViewRes = typedArray.getResourceId(R.styleable.IvyAutoLayout_initialView, INVALID_VIEW_RES);
    }

    private void setEmptyView(TypedArray typedArray){
        emptyViewRes = typedArray.getResourceId(R.styleable.IvyAutoLayout_emptyView, android.R.id.empty);
    }

    private void setContentView(TypedArray typedArray){
        groupViewRes = typedArray.getResourceId(R.styleable.IvyAutoLayout_groupView, android.R.id.empty);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //Hide all of our superfluous views here.
        onInitialLayout();
    }

    /*---------------------ViewGroup and RecyclerView Controllers --------------------------------*/

    private void determineAutoLayoutGroup(){
        if(autoLayoutAsRecyclerView()) return;
        if(autoLayoutAsViewGroup()) return;
    }

    /**
     *   IvyAutoLayout will add its children to a viewgroup.
     */
    private boolean autoLayoutAsViewGroup(){
        View targetView = findViewById(groupViewRes);
        if(targetView instanceof ViewGroup){
            useAutoLayoutAsViewGroup((ViewGroup) targetView);
            return true;
        }
        return false;
    }

    private void useAutoLayoutAsViewGroup(ViewGroup targetView) {
        layoutStratagy = new ViewGroupViewInflationAdapter(targetView);
    }

    /**
     *  IvyAutoLayout will add its children recyclerView.
     */
    private boolean autoLayoutAsRecyclerView(){
        View targetView = findViewById(groupViewRes);
        if(targetView instanceof RecyclerView) {
            useAutoLayoutAsRecyclerView();
            return true;
        }
        return false;
    }

    private void useAutoLayoutAsRecyclerView() {
        IvyRecyclerViewAdapter recyclerViewAdapter;
    }

    /*-----------------------------------------Data Management -----------------------------------*/



    /*------------------------- Empty and Initial View Controlers --------------------------------*/

    private void onInitialLayout(){
        for(int childrenIter = 0; childrenIter < getChildCount(); childrenIter++){
            View child = getChildAt(childrenIter);
            if(child.getId() != initialViewRes) {
                child.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Runs if the call to set the data was called, but no items have been added to the autolayout
     */
    private void onNoItemsAdded(){
        setViewVisibility(emptyViewRes, View.VISIBLE);
    }

    private void onItemsAdded(){
        //Rehide the empty view and the initial view.
        setViewVisibility(emptyViewRes, View.GONE);
        setViewVisibility(initialViewRes, View.GONE);
        setViewVisibility(groupViewRes, View.VISIBLE);
    }

    private void setViewVisibility(@IdRes int viewId, int visibility){
        View emptyView = findViewById(viewId);
        if(emptyView != null){
            emptyView.setVisibility(visibility);
        }
    }
}
