package com.thedailynerd.ivybinding.layout.inflationadapters

import android.view.View
import com.thedailynerd.ivybinding.layout.BindingPair
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewInflationAdapter

/**
 * This class shouldn't ever be used. We should safeguard against this class because we shouldn't allow
 * for our targetConverter to receive a view that isn't a viewgroup or a recyclerview.
 */
class UnsupportedViewInflationAdapter(view: View) : ViewInflationAdapter {

    override fun addView(layout: Int, bindingPair: BindingPair) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}