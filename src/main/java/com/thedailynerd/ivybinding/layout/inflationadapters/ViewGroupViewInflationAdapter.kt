package com.thedailynerd.ivybinding.layout.inflationadapters

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thedailynerd.ivybinding.layout.BindingPair
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewInflationAdapter

/**
 * Is used by autolayout to adapt to different types of views. In this case we allow the adaptation from a ViewGroup
 * to the autolayout
 * Created by Nick on 7/14/16.
 */
class ViewGroupViewInflationAdapter(val targetView: ViewGroup) : ViewInflationAdapter {

    val layoutInflater = LayoutInflater.from((targetView.context))

    override fun addView(layout: Int, bindingPair: BindingPair) {
        val dataBinding = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, layout, targetView, true)
        setBinding(bindingPair, dataBinding)
    }

    private fun setBinding(bindingPair: BindingPair, dataBinding: ViewDataBinding) {
        val renderTarget = bindingPair.renderTarget!!
        val dataBindingVar = renderTarget.targetBindingVar
        val dataBindingData = bindingPair.data
        if (dataBindingVar != null && dataBindingData != null) {
            dataBinding.setVariable(dataBindingVar, dataBindingData)
        }
    }
}
