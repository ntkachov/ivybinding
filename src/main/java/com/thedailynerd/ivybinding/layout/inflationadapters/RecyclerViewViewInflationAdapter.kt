package com.thedailynerd.ivybinding.layout.inflationadapters

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewInflationAdapter
import com.thedailynerd.ivybinding.layout.BindingPair
import com.thedailynerd.ivybinding.layout.recyclerview.IvyRecyclerViewAdapter

/**
 * Created by Nick on 7/23/16.
 */
class RecyclerViewViewInflationAdapter(val view: RecyclerView) : ViewInflationAdapter {

    val ivyRecyclerViewAdapter: IvyRecyclerViewAdapter

    init {
        checkAdapterViewType(view)
        ivyRecyclerViewAdapter = view.adapter as IvyRecyclerViewAdapter? ?: IvyRecyclerViewAdapter()
        setLayoutManagerIfNeeded(view)
    }

    private fun setLayoutManagerIfNeeded(view : RecyclerView){
        if(view.layoutManager == null){
            view.layoutManager = getDefaultLayoutManager()
            view.adapter = ivyRecyclerViewAdapter
        }
    }

    private fun getDefaultLayoutManager() : RecyclerView.LayoutManager {
         return LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
    }

    override fun addView(layout: Int, bindingPair: BindingPair){
        ivyRecyclerViewAdapter.addItem(bindingPair)
    }

    private fun checkAdapterViewType(view: RecyclerView){
        if(view.adapter != null && view.adapter !is IvyRecyclerViewAdapter){
            throw IllegalStateException("RecyclerView should not have an adapter already set. " +
                    "IvyBinding will take care of adding an adapter to the RecyclerView")
        }
    }
}
