package com.thedailynerd.ivybinding.layout.inflationadapters;

import com.thedailynerd.ivybinding.layout.BindingPair;

import org.jetbrains.annotations.NotNull;

/**
 * Is used as a stratagy by AutoLayout to adapt between different types of views.
 * Created by Nick on 7/14/16.
 */
public interface ViewInflationAdapter {

    void addView(int layout, @NotNull BindingPair bindingPair);
}
