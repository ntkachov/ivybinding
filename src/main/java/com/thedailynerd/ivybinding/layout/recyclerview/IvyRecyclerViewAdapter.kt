package com.thedailynerd.ivybinding.layout.recyclerview

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thedailynerd.ivybinding.builder.OnBindListener
import com.thedailynerd.ivybinding.builder.RenderTarget
import com.thedailynerd.ivybinding.layout.BindingPair
import java.util.*

/**
 * Created by Nick on 7/13/16.
 */
class IvyRecyclerViewAdapter() : RecyclerView.Adapter<IvyRecyclerViewAdapter.IvyViewHolder>() {

    lateinit var layoutInflater : LayoutInflater
    val backingList = ArrayList<BindingPair>()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        layoutInflater = LayoutInflater.from(recyclerView.context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IvyViewHolder {
        val inflatedView = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, viewType, parent, false)
        return IvyViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: IvyViewHolder, position: Int) {
        val item = backingList[position]
        val renderTarget = getRenderTargetFromItem(item)
        val bindingVar = renderTarget.targetBindingVar!!
        val data = item.data
        val itemView = holder.itemView

        //TODO: optional configureable failure state? Maybe pass view into the bindingConditional so it can handle its failure case
        if(renderTarget.targetBindConditional?.invoke(data) ?: true) {
            holder.dataBinding.setVariable(bindingVar, data)
            onBind(renderTarget, itemView, data)
            applyOnClickListeners(renderTarget, itemView)
        }
    }

    private fun onBind(renderTarget: RenderTarget, itemView: View, data: Any?){
        renderTarget.targetBindingHandler.forEach{ onBind ->
            onBind.onBind(itemView, data)
        }
    }

    //TODO: Allow the click listener to reference its data.
    fun applyOnClickListeners(renderTarget: RenderTarget, renderedView: View){
        renderedView.setOnClickListener { view ->
            renderTarget.targetClickListener.forEach{ onClickListener ->
                onClickListener.onClick(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return backingList.count()
    }

    private fun getRenderTargetFromItem(binding: BindingPair) : RenderTarget{
        val renderTarget = binding.renderTarget
        if(renderTarget == null){
            throw IllegalStateException("Received a bindingPair with no renderTarget attached.")
        }
        return renderTarget
    }

    override fun getItemViewType(position: Int): Int {
        val layoutId = backingList[position].renderTarget?.targetBindingLayout
        if(layoutId == null) {
            throw IllegalStateException("Received a bindingPair with no renderTarget attached.")
        }
        return layoutId
    }

    fun addItem(binding: BindingPair) {
        backingList.add(binding)
    }

    class IvyViewHolder(viewBinding : ViewDataBinding) : RecyclerView.ViewHolder(viewBinding.root) {
        var dataBinding : ViewDataBinding = viewBinding
    }
}
