package com.thedailynerd.ivybinding.layout

import com.thedailynerd.ivybinding.builder.RenderTarget

/**
 * Created by Nick on 7/21/16.
 */
data class BindingPair(val renderTarget: RenderTarget?, val data: Any?) {

    companion object{
        val NULL = BindingPair(null, null)
    }

    fun isNull() : Boolean{
        return renderTarget == null && data == null
    }

    fun isNotNull(function: (BindingPair) -> Unit) {
        if(!isNull()){
            function(this)
        }
    }

    fun validate(){
        if(renderTarget == null){
            throw IllegalArgumentException("Binding pair doesn't have a renderTarget attached." +
                    " Did you forget to call IvyBuilder.generateBindings()?")
        }
    }
}
