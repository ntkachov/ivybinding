package com.thedailynerd.ivybinding.layout

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.thedailynerd.ivybinding.builder.RenderTarget
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewInflationAdapter
import com.thedailynerd.ivybinding.layout.inflationadapters.UnsupportedViewInflationAdapter
import com.thedailynerd.ivybinding.layout.inflationadapters.RecyclerViewViewInflationAdapter
import com.thedailynerd.ivybinding.layout.inflationadapters.ViewGroupViewInflationAdapter
import java.lang.ref.WeakReference
import java.util.*

/**
 * Master class to convert renderTargets to actual views.
 * Created by Nick on 7/14/16.
 */
class IvyBindingViewGenerator(private val rootLayout: ViewGroup) {//TODO: Cache instance somewhere? Don't keep creating them all over the place.

    private val layoutInflater: LayoutInflater
    private val viewInflaterCache = HashMap<Int, ViewInflationAdapter>() //View.hashCode -> ViewInflationAdapter

    init {
        layoutInflater = LayoutInflater.from(rootLayout.context)
    }

    fun renderAndAttachRenderTargetToParent(bindingPair: BindingPair) {
        val renderTarget = bindingPair.renderTarget!! //Throw if render target reaches this point as a null
        val layout = renderTarget.targetBindingLayout!!
        val targetParent = getTargetParent(renderTarget)
        targetParent.addView(layout, bindingPair)
    }

    private fun getTargetParent(target: RenderTarget): ViewInflationAdapter {
        //When checking null, make sure to grab a local reference to the value you're null checking
        //Otherwise you might NPE if this value changes between your null check and your usage
        val parentViewGroup = target.parentViewGroup

        if(parentViewGroup != null){
            return getViewInflationAdapterForView(parentViewGroup)
        } else {
            return getInflatedAutoAdapter(target)
        }
    }

    fun getInflatedAutoAdapter(target: RenderTarget): ViewInflationAdapter {
        //TODO: Refactor this for nesting layouts. We'll need to reference parents here
        val inflatedParentView = rootLayout.findViewById(target.parentViewId!!)
        if (inflatedParentView is ViewGroup || inflatedParentView is RecyclerView) {
            return getViewInflationAdapterForView(inflatedParentView)
        }else {
            throw IllegalArgumentException("Could not add " + target.targetClass + ". inflateInto view is not a ViewGroup or RecyclerView")
        }
    }

    fun getViewInflationAdapterForView(view: View): ViewInflationAdapter {
        val cachedView = viewInflaterCache[view.hashCode()]
        if(cachedView != null){
            return cachedView
        } else {
            val newInflationAdapter = when (view) {
                is RecyclerView -> RecyclerViewViewInflationAdapter(view)
                is ViewGroup -> ViewGroupViewInflationAdapter(view)
                else -> UnsupportedViewInflationAdapter(view)
            }
            viewInflaterCache.put(view.hashCode(), newInflationAdapter)
            return newInflationAdapter
        }
    }


}



