package com.thedailynerd.ivybinding;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Nick on 7/26/2016.
 */
public class TestingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testing_layout);
    }
}
