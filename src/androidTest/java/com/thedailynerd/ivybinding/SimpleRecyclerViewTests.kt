package com.thedailynerd.ivybinding
import android.support.test.espresso.ViewAssertion
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.test.ViewAsserts
import android.test.suitebuilder.annotation.LargeTest

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import java.util.ArrayList
import java.util.Observable
import java.util.UUID

import android.support.test.espresso.Espresso.*
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.matcher.RootMatchers.*
import android.support.test.espresso.matcher.ViewMatchers
import com.thedailynerd.ivybinding.builder.OnBindListener
import com.thedailynerd.ivybinding.robots.IvyBindingRobot
import org.hamcrest.Matchers.*
import org.junit.runners.JUnit4
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import com.thedailynerd.ivybinding.util.RecyclerViewMatcher.*


import org.junit.Assert.*

/**
 * Created by Nick on 7/26/2016.
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class SimpleRecyclerViewTests {

    internal lateinit var dataList: MutableList<String>
    internal var simpleViewGroupRobot = IvyBindingRobot()
    private val latch = CountDownLatch(1)

    @get:Rule                                                                                       //http://stackoverflow.com/questions/29945087/kotlin-and-new-activitytestrule-the-rule-must-be-public
    var mActivityRule = ActivityTestRule(TestingActivity::class.java)

    @Before
    fun initTestData() {
        dataList = ArrayList<String>()
        for (stringCount in 0..TEST_DATA_LENGTH - 1) {
            dataList.add(UUID.randomUUID().toString())
        }
    }

    @Test
    @Throws(InterruptedException::class)
    fun testAddDataListToViewGroup() {
        simpleViewGroupRobot.getTestRecyclerViewIvy(dataList).generateBindings(mActivityRule.activity)

        Thread.sleep(10)

        dataList.forEachIndexed { index, string ->
            onView(withRecyclerView(R.id.testRecyclerLayout).atPosition(index)).check(ViewAssertions.matches(isDisplayed()))
        }
    }

    /**
     * Sanity check, If this fails theres something wrong with the tests.
     */
    @Test
    fun testSanity() {
        onView(withText(R.string.sanity)).check(matches(isDisplayed()))
    }

    companion object {
        private val TEST_DATA_LENGTH = 10
        private val TEST_TIMEOUT_MS = 2L                                                             //If we timeout for longer than 2ms, we're doing something horribly wrong.
    }
}
