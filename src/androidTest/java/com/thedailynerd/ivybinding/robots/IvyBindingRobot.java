package com.thedailynerd.ivybinding.robots;

import com.thedailynerd.ivybinding.IvyBinder;
import com.thedailynerd.ivybinding.R;
import com.thedailynerd.ivybinding.builder.IvyBuilder;
import com.thedailynerd.ivybinding.builder.IvyBuilderItemBuilder;

import java.util.List;

/**
 * Created by Nick on 7/27/2016.
 */
public class IvyBindingRobot {

    public IvyBuilder getTestViewGroupIvy(List<String> dataList){
        return  getTestRenderingBuilder(dataList).inflateInto(R.id.testGroupLayout);
    }

    public IvyBuilder getTestRecyclerViewIvy(List<String> dataList){
        return getTestRenderingBuilder(dataList).inflateInto(R.id.testRecyclerLayout);
    }

    public IvyBuilderItemBuilder getTestRenderingBuilder(List<String> dataList){
        return IvyBinder.initializeWith(rx.Observable.just(dataList)).render(String.class)
                .bindTo(R.layout.testing_binding, com.thedailynerd.ivybinding.BR.data);
    }

}
